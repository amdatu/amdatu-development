#!/bin/bash
if [ -z "$1" ] 
then
    echo "No argument supplied"
    exit 1
else
  if [ -d "$1" ]
  then
    echo ""
  else
    echo "Not a valiid dir, make sure the dir exists"
    exit 1
  fi
fi

rsync -av \
    --exclude='*.DS_Store' \
    ../workspace/ "$1"

rsync -av \
    --exclude='*.DS_Store' \
    --exclude='adoc/_includes/*' \
    --exclude='build/*' \
    --exclude='generated/*' \
    ../docs/ "$1/docs"

echo "Project created there are a few manual steps remaining"
echo " - Update cnf/ext/amdatu-PROJECT_NAME.bnd"
echo " - Update docs/index.adoc as that's now containing the documentation from this project"