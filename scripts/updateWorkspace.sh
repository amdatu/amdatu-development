#!/bin/bash
if [ -z "$1" ] 
then
    echo "No argument supplied"
    exit 1
else
  if [ -d "$1" ]
  then
    echo ""
  else
    echo "Not a valiid dir"
    exit 1
  fi
fi

rsync -av \
    --exclude='*.DS_Store' \
    --exclude='NOTICE' \
    --exclude='cnf/features/*' \
    --exclude='cnf/project-deps.maven' \
    --exclude='cnf/build.bnd' \
    --exclude='README.md' \
    --exclude='bitbucket-pipelines.yml' \
    --exclude='.gitignore' \
    ../workspace/ "$1"

rsync -av \
    --exclude='*.DS_Store' \
    --existing \
    ../workspace/cnf/features/ "$1/cnf/ext"

rsync -av \
    --exclude='*.DS_Store' \
    --exclude='adoc/_includes/*' \
    --exclude='adoc/index.adoc' \
    --exclude='build/' \
    --exclude='build/*' \
    --exclude='generated*' \
    ../docs/ "$1/docs"