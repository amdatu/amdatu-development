package org.amdatu.development.gradle

import com.amazonaws.SdkClientException
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient
import com.amazonaws.services.cloudfront.model.CreateInvalidationRequest
import com.amazonaws.services.cloudfront.model.InvalidationBatch
import com.amazonaws.services.cloudfront.model.Paths
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.transfer.MultipleFileUpload
import com.amazonaws.services.s3.transfer.TransferManager
import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project

class AmdatuPublishPluginExtension {

    String docsRoot

    String repoRoot

    String project

    String bucket

    String version

    boolean updateLatest

}

/**
 * Gradle plugin that handles publising an Amdatu Project to the amdatu repository
 *
 * Upload documentation
 * Upload repository (release or snapshot)
 * Update latest.xml reference to last release when uploading a released version
 * Invalidate CloudFront cache
 */
class AmdatuPublishPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {

        def extension = project.extensions.create('amdatuPublish', AmdatuPublishPluginExtension)

        project.task('publishRelease') {

            doLast {
                if (extension.project == null) {
                    throw new GradleException('AmdatuPublishPlugin: Required property "project" not set')
                }

                if (extension.version == null) {
                    throw new GradleException('AmdatuPublishPlugin: Required property "version" not set')
                }

                if (extension.bucket == null) {
                    throw new GradleException('AmdatuPublishPlugin: Required property "bucket" not set')
                }

                File docsRootDir
                if (extension.docsRoot != null) {
                    docsRootDir = project.file(extension.docsRoot)
                    if (!docsRootDir.isDirectory()) {
                        throw new GradleException("AmdatuPublishPlugin: [docsRoot: $docsRootDir] is not a directory")
                    }
                } else {
                    throw new GradleException('AmdatuPublishPlugin: Required property "docsRoot" not set')
                }

                File repoRootDir
                if (extension.repoRoot != null) {
                    repoRootDir = project.file(extension.repoRoot)
                    if (!repoRootDir.isDirectory()) {
                        throw new GradleException("AmdatuPublishPlugin: [repoRoot: $repoRootDir] is not a directory")
                    }
                } else {
                    throw new GradleException('AmdatuPublishPlugin: Required property "repoRoot" not set')
                }

                def awsCredentialsProvider = new EnvironmentVariableCredentialsProvider()
                try {
                    awsCredentialsProvider.getCredentials()
                } catch (SdkClientException e) {
                    throw new GradleException("AmdatuPublishPlugin: failed to get AWS credentials!", e)
                }

                TransferManager transferManager = new TransferManager(awsCredentialsProvider)

                logger.info("AmdatuPublishPlugin: Uploading docs from $docsRootDir to s3://${extension.bucket}/${extension.project}/${extension.version}/docs")
                MultipleFileUpload docsUpload = transferManager.uploadDirectory(extension.bucket, "${extension.project}/${extension.version}/docs", docsRootDir, true)

                logger.info("AmdatuPublishPlugin: Upload repo from $repoRootDir to s3://${extension.bucket}/${extension.project}/${extension.version}/repo")
                MultipleFileUpload repoUpload = transferManager.uploadDirectory(extension.bucket, "${extension.project}/${extension.version}/repo", repoRootDir, true)

                // Wait for the uploads to finish
                docsUpload.waitForCompletion()
                repoUpload.waitForCompletion()

                def latestIndexKey = "${extension.project}/latest.xml"

                if (extension.version.startsWith('r') || extension.updateLatest){
                    byte[] latestIndex = """<?xml version='1.0' encoding='utf-8'?> 
<repository name='${extension.project} latest' xmlns='http://www.osgi.org/xmlns/repository/v1.0.0'> 
  <referral url="https://repository.amdatu.org/${extension.project}/${extension.version}/repo/index.xml.gz" /> 
</repository>
""".getBytes('UTF-8')

                    def objectMetadata = new ObjectMetadata()
                    objectMetadata.setContentLength(latestIndex.length)
                    transferManager.upload(extension.bucket, latestIndexKey, new ByteArrayInputStream(latestIndex), objectMetadata)
                }

                AmazonCloudFrontClient cloudFrontClient = new AmazonCloudFrontClient(awsCredentialsProvider)

                // Invalidate CloudFront cache
                def distributionId = System.getenv("CLOUDFRONT_DISTRIBUTION_ID")
                if (distributionId != null) {
                    def invalidatePath = "/${extension.project}/${extension.version}/*"
                    logger.info("AmdatuPublishPlugin: Invalidating CloudFront cache for path $invalidatePath")
                    def invalidationRequest = new CreateInvalidationRequest()
                            .withDistributionId(distributionId)
                            .withInvalidationBatch(new InvalidationBatch()
                            .withCallerReference("Publish: $extension.project($extension.version) at: ${System.currentTimeMillis()}")
                            .withPaths(new Paths()
                            .withItems(invalidatePath, "/$latestIndexKey")
                            .withQuantity(2))
                    )
                    cloudFrontClient.createInvalidation(invalidationRequest)
                } else {
                    logger.info("AmdatuPublishPlugin: No CLOUDFRONT_DISTRIBUTION_ID, not invalidating CloudFront cache")
                }
            }
        }
    }
}

