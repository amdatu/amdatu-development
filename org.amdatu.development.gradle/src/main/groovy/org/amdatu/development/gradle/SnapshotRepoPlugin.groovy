package org.amdatu.development.gradle

import aQute.bnd.build.Workspace
import aQute.bnd.deployer.repository.LocalIndexedRepo
import aQute.bnd.osgi.Jar
import aQute.bnd.service.RepositoryPlugin
import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.BasePlugin
import org.osgi.framework.Version

import java.text.SimpleDateFormat
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import java.util.zip.ZipOutputStream

/**
 * Repository to generate a snapshot repository for a bnd workspace.
 *
 * This requires the project to have a -releaserepo and -baselinerepo configured in the bnd workspace.
 */
class SnapshotRepoPlugin implements Plugin<Project> {

    private Workspace workspace

    @Override
    void apply(Project project) {
        if (project.bndWorkspace == null) {
            throw new GradleException("SnapshotRepoPlugin: project ${project.name} has no 'bndWorkspace' property")
        }

        workspace = new Workspace(project.rootDir)

        project.task('snapshotRepo') {
            dependsOn project.getTasksByName('release', true)

            doLast {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss")
                String postfix = "DEV" + sdf.format(new Date(System.currentTimeMillis()))

                RepositoryPlugin baselineRepo = getRepo('-baselinerepo')
                RepositoryPlugin releaseRepo = getRepo('-releaserepo')

                def snapshotRepoDir = project.file('generated/snapshot-repo')

                if (snapshotRepoDir.exists()) {
                    logger.info("SnapshotRepoPlugin: Snapshot repo dir '$snapshotRepoDir' exists, removing")
                    project.delete(snapshotRepoDir)
                }

                if (!snapshotRepoDir.mkdirs()) {
                    throw new GradleException("SnapshotRepoPlugin: Failed to create snapshot repo dir '$snapshotRepoDir'")
                }


                def snapshotRepo = new LocalIndexedRepo()
                def projectName = workspace.getProperty('projectName')
                logger.info("SnapshotRepoPlugin: Generating snapshot repository for $projectName in $snapshotRepoDir")
                snapshotRepo.setProperties([name: "$projectName snapshot".toString(), local: snapshotRepoDir.getAbsolutePath(), pretty: 'false', ])
                releaseRepo.list().each { bsn ->
                    File baselineJarFile = getLatestFromRepo(baselineRepo, bsn)

                    def baselineVersion = '0.0.0'
                    if (baselineJarFile != null) {
                        baselineVersion = getVersion(baselineJarFile) ?: '0.0.0'
                    }

                    File bundleJarFile = getLatestFromRepo(releaseRepo, bsn)
                    def version = getVersion(bundleJarFile)
                    if (baselineJarFile == null || baselineVersion != version) {
                        def osgiVersion = new Version(baselineVersion)
                        def snapshotVersion
                        if (osgiVersion.getQualifier()== "") {
                            snapshotVersion = new Version(osgiVersion.major, osgiVersion.minor, osgiVersion.micro, postfix).toString()
                        } else {
                            snapshotVersion = new Version(osgiVersion.major, osgiVersion.minor, osgiVersion.micro, "${osgiVersion.qualifier}_${postfix}").toString()
                        }

                        logger.info "SnapshotRepoPlugin: Bundle '${bsn}' has been changed, including snapshot: '${snapshotVersion}' next release version: '${version}'"
                        def tmp = getBundleWithNewVersion(bundleJarFile, snapshotVersion)
                        new FileInputStream(tmp).withStream { stream -> snapshotRepo.put(stream, null) }
                    } else {
                        logger.info "SnapshotRepoPlugin: Bundle '${bsn}' untouched including released version: '${baselineVersion}'"
                        new FileInputStream(baselineJarFile).withStream { stream -> snapshotRepo.put(stream, null) }
                    }

                }
            }
        }

        // Make sure the base plugin has been applied
        project.getPluginManager().apply(BasePlugin.class)

        // Extend the clean task so the generated folder will be removed
        project.clean {
            doLast {
                project.delete "generated"
            }
        }
    }

    File getLatestFromRepo(/* RepositoryPlugin */  baselineRepo, String bsn) {
        def versions = baselineRepo.versions(bsn)
        if (versions.empty) {
            return null
        }
        File released = baselineRepo.get(bsn, versions.last(), [:])
        if (released == null) {
            throw new GradleException("Failed to retrieve baseline jar [bsn: '${bsn}' version: '${version}']")
        }
        return released
    }

    static String getVersion(File jarFile) {
        String version = new Jar(jarFile).withCloseable {
            it.version
        }

        if (version != null) {
            // This wil validate the version and add missing minor / major version parts if needed
            version = Version.valueOf(version).toString()
        }
        return version
    }

    RepositoryPlugin getRepo(repoAttr) {
        def repoName = workspace.getProperty(repoAttr)
        def repos = workspace.getRepositories()
        for (repo in repos) {
            if (repo.name == repoName) {
                return repo
            }
        }
        throw new GradleException("Repo not found, $repoAttr, $repoName, $repos")
    }

    static def getBundleWithNewVersion(File sourceJar, String version) {
        def zip = new ZipFile(sourceJar)
        def zipTemp = File.createTempFile('out', '.jar')
        zipTemp.deleteOnExit()
        new ZipOutputStream(new FileOutputStream(zipTemp)).withStream { zos ->
            for(e in zip.entries()) {
                if(e.name == 'META-INF/MANIFEST.MF') {
                    zip.getInputStream(e).withStream { is ->
                        def br = new BufferedReader(new InputStreamReader(is, 'UTF-8'))
                        zos.putNextEntry(new ZipEntry('META-INF/MANIFEST.MF'))

                        def line
                        while ((line = br.readLine())!=null) {
                            if (line.startsWith("Bundle-Version:")) {
                                zos << "Bundle-Version: $version\r\n".getBytes('UTF8')
                            } else {
                                zos << "$line\r\n".getBytes('UTF8')
                            }
                        }
                    }
                } else {
                    zos.putNextEntry(e)
                    zip.getInputStream(e).withStream {is -> zos << is.bytes }
                }
                zos.closeEntry()
            }
        }
        return zipTemp
    }
}


