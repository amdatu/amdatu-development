# Amdatu Development

This repository contains some useful stuff when devleloping Amdatu components

## Workspace template

The `workspace` folder in the _amdatu-development_ repository contains a workspace template that should be used by all Amdatu projects.

### Workspace update scripts

As the workspace template will change over time there are some scripts to help keep workspaces up to date. 

*createWorkspace.sh*  
*_Usage:_* _./createWorkspace.sh AMDATU_WORKSPACE_DIR_

The create script doesn't create the folder so make sure it exists.

*updateWorkspace.sh*  
*_Usage:_* _./updateWorkspace.sh AMDATU_WORKSPACE_DIR_

This updates the bnd workspace, gradle build scripts and shared documentation files (style, scripts, fonts ..).

## Docker images 

At this moment just a single image to be used for the Bitbucket Pipelines build

### build-jdk8-alpine

The image is based on the `openjdk:8-jdk-alpine` image. 
Additionally it has: 

* bash 
* graphviz, fontconfig, ttf-dejavu _(Required for plantuml diagrams in documentation)_

*Dockerfile*
```
include::../../docker/build-jdk8-alpine/Dockerfile[]
```

*Updating the image*
```
cd docker/build-jdk8-alpine/

# Rebuild the image
docker build -t amdatu/build-jdk8-alpine:latest .

# Push the image
docker push amdatu/build-jdk8-alpine:latest
```